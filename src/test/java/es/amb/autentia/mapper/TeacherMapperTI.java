package es.amb.autentia.mapper;

import java.util.List;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import es.amb.autentia.model.Teacher;

@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml", "classpath:applicationContextTest.xml" })
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class TeacherMapperTI {

    @Inject
    TeacherMapper mapper;

    @Test
    public void shouldCallMapperGetTeachersWhenGetTeachers() {
        List<Teacher> teachers = mapper.getTeachers();
        Assert.assertEquals(4, teachers.size());
        Assert.assertEquals("Andrés Lopez Perez", teachers.get(0).getFullName());
    }
}
