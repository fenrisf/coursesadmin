package es.amb.autentia.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.IsNot.not;

import java.util.List;

import javax.inject.Inject;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import es.amb.autentia.model.Course;

@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml", "classpath:applicationContextTest.xml" })
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class CourseMapperTI {

    @Inject
    CourseMapper mapper;

    @Test
    public void shouldCallMapperGetCoursesWhenGetCourses() {
        List<Course> courses = mapper.getCourses();
        assertThat(courses.size(), is(10));
        assertThat(courses, is(not(empty())));
        assertThat(courses.get(0).getNumHours(), is(250));
        assertThat(courses.get(courses.size() - 1).getTitle(), is("Mastering Scrum"));
    }

    @Test
    public void shouldCallMapperInsertCourseWhenInsertCourse() {
        Course courseToInsert = new Course();
        courseToInsert.setActive("S");
        courseToInsert.setIdTeacher(1);
        courseToInsert.setLevel("Básico");
        courseToInsert.setNumHours(200);
        courseToInsert.setTitle("Curso de Test");
        mapper.insertCourse(courseToInsert);

        List<Course> courses = mapper.getCourses();
        assertEquals(11, courses.size());
        assertEquals("Curso de Test", courses.get(courses.size() - 1).getTitle());
    }
}
