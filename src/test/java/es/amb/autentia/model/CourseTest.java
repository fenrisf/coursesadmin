package es.amb.autentia.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CourseTest {

    private Course courseForTestOne = new Course();

    private Course courseForTestTwo = new Course();

    @Before
    public void initTest() {
        courseForTestOne.setActive("S");
        courseForTestOne.setAgendaName("one.doc");
        courseForTestOne.setId(1);
        courseForTestOne.setIdTeacher(1);
        courseForTestOne.setLevel("Básico");
        courseForTestOne.setNumHours(100);
        courseForTestOne.setTitle("One");

        courseForTestTwo.setActive("N");
        courseForTestTwo.setAgendaName("");
        courseForTestTwo.setId(2);
        courseForTestTwo.setIdTeacher(2);
        courseForTestTwo.setLevel("Avanzado");
        courseForTestTwo.setNumHours(200);
        courseForTestTwo.setTitle("Two");
    }

    @Test
    public void shouldCompareWhenSetCourse() {
        assertNotEquals(courseForTestOne.getAgendaName(), courseForTestTwo.getAgendaName());
        assertNotEquals(courseForTestOne.getActive(), courseForTestTwo.getActive());
        assertNotEquals(courseForTestOne.getId(), courseForTestTwo.getId());
        assertNotEquals(courseForTestOne.getIdTeacher(), courseForTestTwo.getIdTeacher());
        assertNotEquals(courseForTestOne.getLevel(), courseForTestTwo.getLevel());
        assertNotEquals(courseForTestOne.getNumHours(), courseForTestTwo.getNumHours());
        assertNotEquals(courseForTestOne.getTitle(), courseForTestTwo.getTitle());
        assertTrue(courseForTestOne.getAgenda());
        assertFalse(courseForTestTwo.getAgenda());
    }
}
