package es.amb.autentia.model;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TeacherTest {

    private Teacher teacherForTestOne = new Teacher();

    private Teacher teacherForTestTwo = new Teacher();

    @Before
    public void initTest() {
        teacherForTestOne.setId(1);
        teacherForTestOne.setFullName("One");

        teacherForTestTwo.setId(2);
        teacherForTestTwo.setFullName("Two");
    }

    @Test
    public void shouldCompareWhenSetCourse() {
        assertNotEquals(teacherForTestOne.getId(), teacherForTestTwo.getId());
        assertNotEquals(teacherForTestOne.getFullName(), teacherForTestTwo.getFullName());
    }
}
