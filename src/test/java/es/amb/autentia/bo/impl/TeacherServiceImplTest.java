package es.amb.autentia.bo.impl;

import es.amb.autentia.mapper.TeacherMapper;
import es.amb.autentia.model.Teacher;
import es.amb.autentia.service.TeacherService;
import es.amb.autentia.service.impl.TeacherServiceImpl;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TeacherServiceImplTest {

    private TeacherMapper myMapper;

    @Before
    public void initTest() {
        myMapper = mock(TeacherMapper.class);
    }

    @Test
    public void shouldCallTeacherServiceGetTeachersWhenGetTeachers() {
        when(myMapper.getTeachers()).thenReturn(getTeachersForTest());
        TeacherService teacherService = new TeacherServiceImpl(myMapper);

        assertThat(teacherService.getTeachers().size(), is(getTeachersForTest().size()));
    }

    private List<Teacher> getTeachersForTest() {
        Teacher course = new Teacher();
        List<Teacher> teachersList = new ArrayList<>();
        course.setFullName("Teacher name");
        teachersList.add(course);
        return teachersList;
    }
}
