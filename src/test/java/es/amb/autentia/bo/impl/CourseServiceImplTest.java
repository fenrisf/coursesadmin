package es.amb.autentia.bo.impl;

import es.amb.autentia.mapper.CourseMapper;
import es.amb.autentia.model.Course;
import es.amb.autentia.service.CourseService;
import es.amb.autentia.service.impl.CourseServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

public class CourseServiceImplTest {

    private CourseMapper myMapper;

    @Before
    public void initTest() {
        myMapper = mock(CourseMapper.class);
    }

    @Test
    public void shouldCallServiceGetCoursesWhenGetCourses() {
        when(myMapper.getCourses()).thenReturn(getCoursesForTest());
        CourseService courseService = new CourseServiceImpl(myMapper);
        assertThat(courseService.getCourses().size(), is(getCoursesForTest().size()));
        verify(myMapper, times(1)).getCourses();
    }

    @Test
    public void shouldCallMapperInsertCourseWhenGetCourses() {
        Course courseForTest = new Course();
        CourseService courseService = new CourseServiceImpl(myMapper);
        courseService.insertCourse(courseForTest);
        verify(myMapper, times(1)).insertCourse(courseForTest);
    }

    private List<Course> getCoursesForTest() {
        Course course = new Course();
        List<Course> coursesList = new ArrayList<>();
        course.setTitle("Title 1");
        coursesList.add(course);
        return coursesList;
    }
}
