package es.amb.autentia.bean;

import es.amb.autentia.model.Teacher;
import es.amb.autentia.service.CourseService;
import es.amb.autentia.service.TeacherService;
import es.amb.autentia.service.impl.CourseServiceImpl;
import es.amb.autentia.service.impl.TeacherServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.primefaces.model.UploadedFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class CoursesFromViewTest {

    @SuppressWarnings("serial")
    private class CoursesFromViewForTest extends CoursesFormView {

        private boolean called = false;

        public CoursesFromViewForTest(CourseService courseService, TeacherService teacherService) {
            super(courseService, teacherService);
        }

        @Override
        protected void writeMessage(String message) {
            called = true;
        }

        @Override
        protected void copyFile() throws IOException {
        }

        public boolean isCalled() {
            return called;
        }
    }

    private CoursesFromViewForTest classForTest;

    private CourseService courseServiceForTest;

    private TeacherService teacherServiceForTest;

    @Before
    public void initTest() {
        courseServiceForTest = mock(CourseServiceImpl.class);
        teacherServiceForTest = mock(TeacherServiceImpl.class);
        classForTest = new CoursesFromViewForTest(courseServiceForTest, teacherServiceForTest);
        initForm();
    }

    @Test
    public void shouldCallInsertCourseWhenInsertCourse() {
        classForTest.saveCourse();
        verify(courseServiceForTest, times(1)).insertCourse(classForTest.getNewCourse());
        Assert.assertEquals(true, classForTest.isCalled());
    }

    @Test
    public void shouldCallInsertCourseWhenInsertCourseAgenda() {
        UploadedFile mock = mock(UploadedFile.class);
        when(mock.getFileName()).thenReturn("myFile");
        classForTest.setAgenda(mock);
        classForTest.saveCourse();
        verify(courseServiceForTest, times(1)).insertCourse(classForTest.getNewCourse());
        Assert.assertEquals("myFile", classForTest.getNewCourse().getAgendaName());
        Assert.assertEquals("myFile", classForTest.getAgenda().getFileName());
        Assert.assertEquals(true, classForTest.getNewCourse().getAgenda());
        Assert.assertEquals(true, classForTest.isCalled());
    }

    @Test
    public void shouldMatchesWhenInitCourseProperties() {
        Assert.assertEquals("Test Title", classForTest.getTitle());
        Assert.assertEquals(true, classForTest.getActive());
        Assert.assertEquals(1, classForTest.getIdTeacher());
        Assert.assertEquals("Avanzado", classForTest.getLevel());
        Assert.assertEquals(150, classForTest.getNumHours());
    }

    @Test
    public void shoulCallGetTeachersWhenGetTeachers() {
        when(teacherServiceForTest.getTeachers()).thenReturn(getTeachersForTest());
        Assert.assertEquals(classForTest.getTeachers().size(), getTeachersForTest().size());
    }

    private List<Teacher> getTeachersForTest() {
        Teacher course = new Teacher();
        List<Teacher> teachersList = new ArrayList<>();
        course.setFullName("Teacher name");
        teachersList.add(course);
        return teachersList;
    }

    private void initForm() {
        classForTest.setTitle("Test Title");
        classForTest.setActive(true);
        classForTest.setIdTeacher(1);
        classForTest.setLevel("Avanzado");
        classForTest.setNumHours(150);
    }
}
