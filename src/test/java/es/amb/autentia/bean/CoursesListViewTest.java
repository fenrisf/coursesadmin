package es.amb.autentia.bean;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.primefaces.model.DefaultStreamedContent;

import es.amb.autentia.model.Course;
import es.amb.autentia.service.CourseService;
import es.amb.autentia.service.impl.CourseServiceImpl;

@SuppressWarnings("deprecation")
public class CoursesListViewTest {

    @SuppressWarnings("serial")
    private class CoursesListViewForTest extends CoursesListView {

        private boolean called = false;

        public CoursesListViewForTest(CourseService courseService) {
            super(courseService);
        }

        @Override
        protected InputStream getInputStream() {
            ByteArrayInputStream sbis = new ByteArrayInputStream("Prueba".getBytes());
            this.called = true;
            return sbis;
        }

        public boolean isCalled() {
            return called;
        }
    }

    private CourseService courseServiceForTest;

    @Before
    public void initTest() {
        courseServiceForTest = mock(CourseServiceImpl.class);
    }

    @Test
    public void shoulCallGetCoursesWhenGetCourse() {
        when(courseServiceForTest.getCourses()).thenReturn(getCoursesForTest());
        CoursesListView classForTest = new CoursesListView(courseServiceForTest);
        classForTest.init();
        Assert.assertEquals(classForTest.getCourses().size(), getCoursesForTest().size());
    }

    @Test
    public void shoulCallDownloadActionWhenDownloadAction() {
        CoursesListViewForTest classForTest = new CoursesListViewForTest(courseServiceForTest);
        classForTest.setSelectedCourse(getCoursesForTest().get(0));
        classForTest.downloadAction();
        Assert.assertEquals(classForTest.getdFile().getContentType(),
                new DefaultStreamedContent(classForTest.getInputStream()).getContentType());
        Assert.assertTrue(classForTest.isCalled());
    }

    private List<Course> getCoursesForTest() {
        Course course = new Course();
        List<Course> coursesList = new ArrayList<>();
        course.setTitle("Title 1");
        course.setAgendaName("agendaName");
        coursesList.add(course);
        return coursesList;
    }
}
