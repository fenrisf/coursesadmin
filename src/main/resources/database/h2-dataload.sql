INSERT INTO teacher (full_name) VALUES ('Andrés Lopez Perez');
INSERT INTO teacher (full_name) VALUES ('Francisco Perez Alegría');
INSERT INTO teacher (full_name) VALUES ('Alberto Martínez Baudet');
INSERT INTO teacher (full_name) VALUES ('Koji Kabuto');

INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('Spring Avanzado','Avanzado', 'S', 250, 1);
INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('Spring Básico','Básico', 'S', 150, 2);
INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('JPA for dummies','Básico', 'S', 100, 1);
INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('Microservices','Intermedio', 'S', 200, 3);
INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('Inglés Básico','Básico', 'S', 100, 1);
INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('Inglés Medio','Medio', 'S', 150, 4);
INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('Inglés Avanzado','Avanzado', 'N', 250, 4);
INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('Wordpress, crea tu web','Básico', 'S', 150, 1);
INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('Hibernate + Spring','Avanzado', 'N', 200, 2);
INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('Presentaciones eficaces','Intermedio', 'S', 50, 1);
INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('Scrum from scrach','Intermedio', 'S', 95, 3);
INSERT INTO course (title, level, active, num_hours, id_teacher) VALUES ('Mastering Scrum','Avanzado', 'S', 200, 3);



