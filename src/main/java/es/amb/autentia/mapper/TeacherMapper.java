package es.amb.autentia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import es.amb.autentia.model.Teacher;

@FunctionalInterface
public interface TeacherMapper {

    @Select("SELECT id, full_name as fullName FROM teacher")
    public List<Teacher> getTeachers();
}
