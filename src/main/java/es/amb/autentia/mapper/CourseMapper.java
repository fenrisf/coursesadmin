package es.amb.autentia.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import es.amb.autentia.model.Course;

public interface CourseMapper {	
	@Insert("INSERT INTO course (title, level, active, num_hours, id_teacher, agenda_name) "
			+ "VALUES (#{title},#{level}, #{active}, #{numHours}, #{idTeacher}, #{agendaName})")
	@Options(useGeneratedKeys = true, keyProperty = "id", flushCache = true, keyColumn = "id")
	public void insertCourse(Course course);

	@Select("SELECT id, title, level, active, num_hours as numHours, id_teacher as idTeacher, agenda_name as agendaName  "
			+ "FROM course WHERE active='S'")
	public List<Course> getCourses();
}
