package es.amb.autentia.service;

import java.io.Serializable;
import java.util.List;

import es.amb.autentia.model.Teacher;

@FunctionalInterface
public interface TeacherService extends Serializable {

    public List<Teacher> getTeachers();
}
