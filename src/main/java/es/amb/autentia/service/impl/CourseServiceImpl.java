package es.amb.autentia.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.amb.autentia.mapper.CourseMapper;
import es.amb.autentia.model.Course;
import es.amb.autentia.service.CourseService;

@SuppressWarnings("serial")
@Service("courseService")
public class CourseServiceImpl implements CourseService, Serializable {

    private transient CourseMapper mapper;

    @Autowired
    public CourseServiceImpl(CourseMapper mapper) {
        super();
        this.mapper = mapper;
    }

    @Override
    public List<Course> getCourses() {
        return mapper.getCourses();
    }

    @Override
    public void insertCourse(Course course) {
        mapper.insertCourse(course);
    }
}