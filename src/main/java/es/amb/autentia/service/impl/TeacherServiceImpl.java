package es.amb.autentia.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.amb.autentia.mapper.TeacherMapper;
import es.amb.autentia.model.Teacher;
import es.amb.autentia.service.TeacherService;

@SuppressWarnings("serial")
@Service("teacherService")
public class TeacherServiceImpl implements TeacherService, Serializable {

    private transient TeacherMapper mapper;

    @Autowired
    public TeacherServiceImpl(TeacherMapper mapper) {
        super();
        this.mapper = mapper;
    }

    @Override
    public List<Teacher> getTeachers() {
        return mapper.getTeachers();
    }
}
