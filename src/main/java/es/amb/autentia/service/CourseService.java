package es.amb.autentia.service;

import java.io.Serializable;
import java.util.List;

import es.amb.autentia.model.Course;

public interface CourseService extends Serializable {

    public List<Course> getCourses();

    public void insertCourse(Course course);
}
