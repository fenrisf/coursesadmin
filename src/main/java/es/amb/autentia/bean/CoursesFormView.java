package es.amb.autentia.bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.h2.util.IOUtils;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import es.amb.autentia.model.Course;
import es.amb.autentia.model.Teacher;
import es.amb.autentia.service.CourseService;
import es.amb.autentia.service.TeacherService;

@SuppressWarnings("serial")
@ManagedBean(name = "courseForm")
@RequestScoped
public class CoursesFormView implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(CoursesFormView.class);

    private String title;

    private boolean active = true;

    private String level;

    private int numHours = 25;

    private int idTeacher;

    private static final String FILE_DESTINATION = "/Users/amartinez/courses_docs";

    private transient UploadedFile agenda;

    private Course newCourse;

    @ManagedProperty("#{courseService}")
    CourseService courseService;

    @ManagedProperty("#{teacherService}")
    TeacherService teacherService;

    public CoursesFormView() {
        super();
    }

    public CoursesFormView(CourseService courseService, TeacherService teacherService) {
        super();
        this.courseService = courseService;
        this.teacherService = teacherService;
    }

    public List<Teacher> getTeachers() {
        return teacherService.getTeachers();
    }

    public void saveCourse() {
        newCourse = new Course();
        newCourse.setActive(this.active ? "S" : "N");
        newCourse.setLevel(this.level);
        newCourse.setNumHours(this.numHours);
        newCourse.setIdTeacher(this.idTeacher);
        newCourse.setTitle(this.title);
        if (agenda != null && !StringUtils.isEmpty(agenda.getFileName())) {
            newCourse.setAgendaName(agenda.getFileName());
            try {
                copyFile();
            } catch (IOException e) {
                this.writeMessage("Problemas al subir el fiechero " + agenda.getFileName() + ".");
                logger.error("Problemas al subir el fichero asociado al curso.", e);
            }
        }

        courseService.insertCourse(newCourse);

        this.writeMessage("El curso " + title + " añadido correctamente.");

        this.resetCourse();
    }

    private void resetCourse() {
        this.active = true;
        this.numHours = 25;
        this.title = "";
        this.idTeacher = -1;
        this.level = null;
    }

    protected void copyFile() throws IOException {
        try {
            File mainDir = new File(FILE_DESTINATION);
            if (!mainDir.exists()) {
                mainDir.mkdir();
            }
            FileOutputStream fos = new FileOutputStream(new File(mainDir, agenda.getFileName()));
            IOUtils.copy(agenda.getInputstream(), fos);
        } catch (IOException e) {
            throw e;
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getNumHours() {
        return numHours;
    }

    public void setNumHours(int numHours) {
        this.numHours = numHours;
    }

    public int getIdTeacher() {
        return idTeacher;
    }

    public void setIdTeacher(int idTeacher) {
        this.idTeacher = idTeacher;
    }

    public UploadedFile getAgenda() {
        return agenda;
    }

    public void setAgenda(UploadedFile agenda) {
        this.agenda = agenda;
    }

    public CourseService getCourseService() {
        return courseService;
    }

    public void setCourseService(CourseService courseService) {
        this.courseService = courseService;
    }

    public TeacherService getTeacherService() {
        return teacherService;
    }

    public void setTeacherService(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    protected void writeMessage(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(message));
    }

    public Course getNewCourse() {
        return newCourse;
    }
}
