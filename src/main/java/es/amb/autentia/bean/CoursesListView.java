package es.amb.autentia.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.amb.autentia.model.Course;
import es.amb.autentia.service.CourseService;

@SuppressWarnings("serial")
@ManagedBean(name = "courseList")
@ViewScoped
public class CoursesListView implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(CoursesListView.class);

    private List<Course> courses;

    private Course selectedCourse;

    private static final String FILE_DESTINATION = "/Users/amartinez/courses_docs";

    private transient StreamedContent dFile;

    @ManagedProperty("#{courseService}")
    CourseService courseService;

    public CoursesListView() {
        super();
    }

    public CoursesListView(CourseService courseService) {
        super();
        this.courseService = courseService;
    }

    @PostConstruct
    public void init() {
        courses = courseService.getCourses();
    }

    public void downloadAction() {
        dFile = new DefaultStreamedContent(getInputStream());
    }

    protected InputStream getInputStream() {
        File tempFile = new File(FILE_DESTINATION + selectedCourse.getAgendaName());
        try {
            return new FileInputStream(tempFile);
        } catch (FileNotFoundException e) {
            logger.error("Problemas al descargar el fichero asociado al curso.", e);
        }
        return null;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public Course getSelectedCourse() {
        return selectedCourse;
    }

    public void setSelectedCourse(Course selectedCourse) {
        this.selectedCourse = selectedCourse;
    }

    public StreamedContent getdFile() {
        return dFile;
    }

    public void setdFile(StreamedContent dFile) {
        this.dFile = dFile;
    }

    public void setCourseService(CourseService courseService) {
        this.courseService = courseService;
    }

    public CourseService getCourseService() {
        return courseService;
    }
}
