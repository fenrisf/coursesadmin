package es.amb.autentia.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Teacher implements Serializable{
	private int id;
	private String fullName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
