package es.amb.autentia.model;

import java.io.Serializable;

import org.springframework.util.StringUtils;

@SuppressWarnings("serial")
public class Course implements Serializable {

    private int id;

    private String title;

    private String active;

    private String level;

    private Integer numHours;

    private Integer idTeacher;

    private String agendaName;

    public Integer getNumHours() {
        return numHours;
    }

    public void setNumHours(Integer numHours) {
        this.numHours = numHours;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getIdTeacher() {
        return idTeacher;
    }

    public void setIdTeacher(Integer idTeacher) {
        this.idTeacher = idTeacher;
    }

    public String getAgendaName() {
        return agendaName;
    }

    public void setAgendaName(String agendaName) {
        this.agendaName = agendaName;
    }

    public boolean getAgenda() {
        if (!StringUtils.isEmpty(getAgendaName())) {
            return true;
        }
        return false;
    }
}
